# Weather application
This is application which display weather details for 5 big European cities (hard coded: 'Skopje', 'London', 'Paris', 'Madrid', 'Belgrade'),
on a click the button 'Hourly forecast' on each city details it shows chart representation of 12 hours forecast for the target city.

## Architecture of the solution
The application is following layered architecture (Core layer -> Abstraction layer -> Presentation layer)
* **Core layer** - It is consisted of reactive store (made with Behaviour Subjects, for bigger projects
it can be done with some Redux implementation - NgRx for Angular) and API services (used for retrieving
data from the REST API)
* **Abstraction layer** - This layer is facade (Facade design pattern) of core layer and 
mediator (Mediator facade pattern) between Core and Presentation layer
* **Presentation layer** - This layer it is consisted from Angular components and its role is 
just to display the data

Vertically the solution it is divided into one **feature module (WeatherModule)** which is lazy-loaded 
module (requested on demand, but for better performance preloading is used) - for this kind of size of applications this is not required, but it is good practice to start developing on that way. 
Beside **WeatherModule** there are two other helper modules: **CoreModule** and **SharedModule**.
* **CoreModule** - This is a module which should be only once imported in **AppModule**, and it consists
all services which have only one instance, models, constants, interceptors and store. 
* **SharedModule** - Here we can have all components/directives/pipes which can be used in several places
in the application. In this application like shared component is wrapper component of ChartJS npm dependency.

The solution follows Smart-Dumb(Container-Presentational) components pattern. 

The application has Responsive design, this is achieved with Twitter Bootstrap 4

Like API for retrieving data it is used: https://openweathermap.org/api
## Technologies
* Angular 10.0.14
* Chart.js - https://www.chartjs.org/
* Twitter Bootstrap for the UI - https://getbootstrap.com/docs/4.5/getting-started/introduction/
* Font-Awesome icons - https://fontawesome.com/v4.7.0/
## Clone bitbucket repo
`$ git clone https://ilija_ilijev@bitbucket.org/ilija_ilijev/weatherapp.git` <br/>
`$ cd weatherapp`

## Instructions for starting
The application can be started in two ways:
* Docker <br/>
    1. Have docker installed locally - https://www.docker.com/products/docker-desktop
    2. Run:  `docker build -t weatherapp .`
    3. Then run: `docker run --name weather-app--container -d -p 80:80 weatherapp`
    4. Open the url http://localhost:80/weather
        
* Run locally <br/>
    1. Have installed NodeJS 10+ version - https://nodejs.org/en/
    2. Install Angular globally with: `npm install -g @angular/cli`
    3. Run: `npm install`
    4. Run: `ng serve --open` - The application will be open in a browser, if not navigate to:  [http://localhost:4200]( [http://localhost:4200])
       

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Linting the application

Run `ng lint`
