import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {ChartsWrapperComponent} from './components/charts-wrapper/charts-wrapper.component';
import {ChartsModule} from 'ng2-charts';
import {SafeUrlPipe} from './pipes/safe-url.pipe';


@NgModule({
  declarations: [ChartsWrapperComponent, SafeUrlPipe],
  imports: [
    CommonModule,
    ChartsModule
  ],
  exports: [
    CommonModule,

    ChartsWrapperComponent,
    SafeUrlPipe
  ],
  providers: [DatePipe]
})
export class SharedModule { }
