import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions} from 'chart.js';

/**
 * Wrapper component for ng2-chart dependency - Facade pattern
 *
 * With this approach if chart in future is used on several places (components),
 * it can be easily replaced with some other dependency with adjusting just the API to fit the new chart implementation,
 * instead of changing on all places where it is used
 *
 * This is limited version of ng2-chart dependency, it can be easily extended with new features which ng2-chart provides,
 * by principle Open for extension, Closed for modification
 *
 */
@Component({
  selector: 'app-charts-wrapper',
  templateUrl: './charts-wrapper.component.html',
  styleUrls: ['./charts-wrapper.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartsWrapperComponent implements OnInit {

  chartOptions: ChartOptions;
  chartJSDataSets: ChartDataSets[] = [];


  @Input() chartLabels: string[];

  /**
   * Currently works only with one data-set
   */
  @Input() chartDataSet: any[];
  @Input() responsive: boolean;
  @Input() chartType: 'line' | 'bar' | 'radar' | 'doughnut' | 'polarArea' | 'pie' | 'scatter';
  @Input() showChartLegend: boolean;
  @Input() lineRounded: boolean;
  @Input() fill: boolean;
  @Input() chartColor: string;
  @Input() chartTitle: string;

  constructor() {
  }

  ngOnInit(): void {
    this.chartJSDataSets = [{
      data: this.chartDataSet,
      lineTension: this.lineRounded ? 0.2 : 0,
      fill: this.fill,
      borderColor: this.chartColor,
      label: this.chartTitle
    }];
    this.chartOptions = {
      responsive: this.responsive
    };
  }
}
