import {NgModule} from '@angular/core';
import {WeatherRootComponent} from './components/container/weather-root/weather-root.component';
import {WeatherRoutingModule} from './weather-routing.module';
import {CityDetailsComponent} from './components/presenter/city-details/city-details.component';
import {WeatherFacadeService} from './services/weather-facade.service';
import {WeatherHourlyForecastComponent} from './components/container/weather-hourly-forecast/weather-hourly-forecast.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [WeatherRootComponent, CityDetailsComponent, WeatherHourlyForecastComponent],
  imports: [
    WeatherRoutingModule,
    SharedModule
  ],
  providers: [WeatherFacadeService]
})
export class WeatherModule { }
