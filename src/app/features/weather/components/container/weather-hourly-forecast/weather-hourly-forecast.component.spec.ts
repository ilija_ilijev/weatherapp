import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WeatherHourlyForecastComponent} from './weather-hourly-forecast.component';
import {RouterTestingModule} from '@angular/router/testing';
import {WeatherFacadeService} from '../../../services/weather-facade.service';
import {DatePipe} from '@angular/common';
import {SharedModule} from '../../../../../shared/shared.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('WeatherHourlyForecastComponent', () => {
  let component: WeatherHourlyForecastComponent;
  let fixture: ComponentFixture<WeatherHourlyForecastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule],
      declarations: [WeatherHourlyForecastComponent],
      providers: [WeatherFacadeService, DatePipe]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherHourlyForecastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
