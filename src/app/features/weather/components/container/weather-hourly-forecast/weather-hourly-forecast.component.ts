import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {WeatherFacadeService} from '../../../services/weather-facade.service';
import {Observable} from 'rxjs';
import {ChartConfig} from '../../../../../core/models/chart-config';

/**
 * Smart (Container) component - Responsibilities:
 *   * communicates with Core layer (via facades - Abstraction layer)
 *   * pass data to dumb (presentational) components
 *   * reacts to events (outputs) from dumb components
 *   * in this situation this is routable component - (but this is not the situation always)
 */
@Component({
  selector: 'app-weather-hourly-forecast',
  templateUrl: './weather-hourly-forecast.component.html',
  styleUrls: ['./weather-hourly-forecast.component.css']
})
export class WeatherHourlyForecastComponent implements OnInit {

  chartConfig$: Observable<ChartConfig>;
  currentCity: string;

  constructor(private _activatedRoute: ActivatedRoute,
              private _weatherFacade: WeatherFacadeService) {
    this.chartConfig$ = this._weatherFacade.getChartConfig$();
  }

  ngOnInit(): void {
    this._weatherFacade.loadHourlyWeatherForecast(window?.history?.state?.coordinates);
    this.currentCity = this._activatedRoute.snapshot.paramMap.get('city');
  }

}
