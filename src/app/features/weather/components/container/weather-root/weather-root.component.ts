import {Component, OnInit} from '@angular/core';
import {WeatherFacadeService} from '../../../services/weather-facade.service';
import {CityWeatherDetails} from '../../../../../core/models/city-weather-details';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

/**
 * Smart (Container) component - Responsibilities:
 *   * communicates with Core layer (via facades - Abstraction layer)
 *   * pass data to dumb (presentational) components
 *   * reacts to events (outputs) from dumb components
 *   * in this situation this is routable component - (but this is not the situation always)
 */
@Component({
  selector: 'app-weather-root',
  templateUrl: './weather-root.component.html',
  styleUrls: ['./weather-root.component.css']
})
export class WeatherRootComponent implements OnInit {

  citiesWeatherDetails$: Observable<CityWeatherDetails[]>;
  availableCities: string[] = ['Skopje', 'London', 'Paris', 'Madrid', 'Belgrade'];

  constructor(private _weatherFacade: WeatherFacadeService,
              private _router: Router) {
    this.citiesWeatherDetails$ = this._weatherFacade.getCitiesWeather$();
  }

  ngOnInit(): void {
    this._weatherFacade.clearCitiesWeatherList();
    this.availableCities.forEach((cityName: string) => {
      this._weatherFacade.addCityWeatherDetails(cityName);
    });
  }

  hourlyForecastEventHandler(cityWeatherDetails: CityWeatherDetails): void {
    this._router.navigate(['weather', 'forecast', cityWeatherDetails.name], {state: {coordinates: cityWeatherDetails.coordinates}});
  }

}
