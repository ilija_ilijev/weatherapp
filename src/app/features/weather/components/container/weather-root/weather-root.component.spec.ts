import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WeatherRootComponent} from './weather-root.component';
import {WeatherFacadeService} from '../../../services/weather-facade.service';
import {DatePipe} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import {CityDetailsComponent} from '../../presenter/city-details/city-details.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('WeatherRootComponent', () => {
  let component: WeatherRootComponent;
  let fixture: ComponentFixture<WeatherRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [WeatherRootComponent, CityDetailsComponent],
      providers: [WeatherFacadeService, DatePipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
