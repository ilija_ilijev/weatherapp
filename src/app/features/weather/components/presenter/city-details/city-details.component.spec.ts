import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CityDetailsComponent} from './city-details.component';
import {CityWeatherDetails} from '../../../../../core/models/city-weather-details';

describe('CityDetailsComponent', () => {
  let component: CityDetailsComponent;
  let fixture: ComponentFixture<CityDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CityDetailsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set cityWeatherDetails input', () => {
    const cityWeatherDetails = {
      name: 'Skopje',
      country: 'MK',
      coordinates: {
        lat: 42,
        lon: 21.43
      },
      weather: 'Clear',
      windSpeed: 10,
      averageTemperature: 10,
      dt: 1121212
    } as CityWeatherDetails;
    component.cityWeatherDetails = cityWeatherDetails;
    fixture.detectChanges();
    expect(component.cityWeatherDetails).toEqual(cityWeatherDetails);
  });

  it('should emit value in hourlyForecast output', () => {
    const cityWeatherDetails = {
      name: 'Skopje',
      country: 'MK',
      coordinates: {
        lat: 42,
        lon: 21.43
      },
      weather: 'Clear',
      windSpeed: 10,
      averageTemperature: 10,
      dt: 1121212
    } as CityWeatherDetails;

    component.hourlyForecast.subscribe((value) => {
      expect(value).toEqual(cityWeatherDetails);
    });
    component.hourlyForecast.emit(cityWeatherDetails);
  });
});
