import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CityWeatherDetails} from '../../../../../core/models/city-weather-details';

/**
 *  Dumb (Presentational) component - this component only presents data retrieved via Input and triggers actions via Outputs
 *  No business logic or communication with core or abstraction layer
 */
@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: ['./city-details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CityDetailsComponent implements OnInit {

  @Input() cityWeatherDetails: CityWeatherDetails;
  @Output() hourlyForecast: EventEmitter<CityWeatherDetails> = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }
}
