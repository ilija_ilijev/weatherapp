import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WeatherRootComponent} from './components/container/weather-root/weather-root.component';
import {WeatherHourlyForecastComponent} from './components/container/weather-hourly-forecast/weather-hourly-forecast.component';

const weatherRoutes: Routes = [
  {
    path: '',
    component: WeatherRootComponent,
  },
  {
    path: 'forecast/:city',
    component: WeatherHourlyForecastComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(weatherRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class WeatherRoutingModule {
}
