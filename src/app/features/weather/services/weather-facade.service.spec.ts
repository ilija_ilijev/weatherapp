import {TestBed} from '@angular/core/testing';

import {WeatherFacadeService} from './weather-facade.service';
import {HttpClientModule} from '@angular/common/http';
import {CityWeatherDetails} from '../../../core/models/city-weather-details';
import {of} from 'rxjs';
import {HourForecastDetails} from '../../../core/models/hour-forecast-details';
import {ChartConfig} from '../../../core/models/chart-config';
import {ChartConfigBuilder} from '../../../core/models/builders/chart-config-builder';
import {AppConstants} from '../../../core/constants/app-constants';
import {DatePipe} from '@angular/common';
import {Coordinates} from '../../../core/models/coordinates';

describe('WeatherFacadeService', () => {
  let service: WeatherFacadeService;
  let weatherFacadeServiceSpy;
  let datePipe: DatePipe;
  beforeEach(() => {
    weatherFacadeServiceSpy = jasmine.createSpyObj<WeatherFacadeService>('WeatherFacadeService',
      [
        'getCitiesWeather$',
        'getChartConfig$',
        'loadHourlyWeatherForecast',
        'addCityWeatherDetails',
        'clearCitiesWeatherList',
        'getHourlyForecast'
      ]);
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [{
        provide: WeatherFacadeService,
        useValue: weatherFacadeServiceSpy
      },
        DatePipe]
    });

    service = TestBed.inject(WeatherFacadeService);
    datePipe = TestBed.inject(DatePipe);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call loadHourlyWeatherForecast only once', () => {
    weatherFacadeServiceSpy.loadHourlyWeatherForecast({
      lat: 10,
      lon: 15
    } as Coordinates);
    expect(weatherFacadeServiceSpy.loadHourlyWeatherForecast.calls.count())
      .toBe(1, 'loadHourlyWeatherForecast should be called once');
  });

  it('should call addCityWeatherDetails only once', () => {
    weatherFacadeServiceSpy.addCityWeatherDetails({});
    expect(weatherFacadeServiceSpy.addCityWeatherDetails.calls.count())
      .toBe(1, 'addCityWeatherDetails should be called once');
  });

  it('should clear city weather details', () => {
    service.clearCitiesWeatherList();

    weatherFacadeServiceSpy.getCitiesWeather$.and.returnValue(of([]));

    service.getCitiesWeather$().subscribe(response => {
      expect(response).toEqual([]);
    });
  });

  it('should get cities weather details', () => {
    const cityWeatherDetails: CityWeatherDetails = {
      name: 'Skopje',
      country: 'MK',
      coordinates: {
        lat: 42,
        lon: 21.43
      },
      weather: 'Clear',
      windSpeed: 10,
      averageTemperature: 10,
      dt: 1121212
    } as CityWeatherDetails;

    weatherFacadeServiceSpy.getCitiesWeather$.and.returnValue(of([cityWeatherDetails]));

    service.getCitiesWeather$().subscribe(response => {
      expect(response).toEqual([cityWeatherDetails]);
    });
  });

  it('should get hourly forecast details', () => {
    const hourForecastDetails: HourForecastDetails[] = [{
      dt: 1121212,
      temperature: 7
    }, {
      dt: 1121315,
      temperature: 10
    }
    ] as HourForecastDetails[];

    weatherFacadeServiceSpy.getHourlyForecast.and.returnValue(of(hourForecastDetails));

    service.getHourlyForecast().subscribe(response => {
      expect(response).toEqual(hourForecastDetails);
    });
  });

  it('should get chart config details', () => {
    const chartConfigData: ChartConfig = new ChartConfigBuilder()
      .setChartColor(AppConstants.DEFAULT_CHART_COLOR)
      .setChartData([7, 10])
      .setLabels([
        datePipe.transform(1607383680 * 1000, AppConstants.DISPLAY_DATE_FORMAT),
        datePipe.transform(1607382000 * 1000, AppConstants.DISPLAY_DATE_FORMAT)
      ])
      .setChartType('line')
      .setChartTitle('Temperature in Celsius degrees')
      .setFill(false)
      .setLineRounded(false)
      .setResponsive(true)
      .setShowLegend(true)
      .build();

    weatherFacadeServiceSpy.getChartConfig$.and.returnValue(of(chartConfigData));

    service.getChartConfig$().subscribe(response => {
      expect(response).toEqual(chartConfigData);
    });
  });
});
