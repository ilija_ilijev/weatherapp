import {Injectable} from '@angular/core';
import {WeatherApiService} from '../../../core/api-services/weather-api.service';
import {WeatherStateService} from '../../../core/store/weather-state.service';
import {CityWeatherDetails} from '../../../core/models/city-weather-details';
import {Observable} from 'rxjs';
import {HourForecastDetails} from '../../../core/models/hour-forecast-details';
import {ChartConfig} from '../../../core/models/chart-config';
import {Coordinates} from '../../../core/models/coordinates';

/**
 *  Behaves like facade of core layer (store and api services)
 *  and mediator between presentational layer (components) and core layer (store and api services)
 *
 *  Applies Facade and Mediator design patterns.
 */
@Injectable()
export class WeatherFacadeService {

  constructor(private _weatherApiService: WeatherApiService,
              private _weatherStoreService: WeatherStateService) {

  }

  /**
   * Just export store values to presentation layer
   */
  getCitiesWeather$(): Observable<CityWeatherDetails[]> {
    return this._weatherStoreService.citiesWeather$();
  }

  getHourlyForecast(): Observable<HourForecastDetails[]> {
    return this._weatherStoreService.hoursForecast$();
  }

  getChartConfig$(): Observable<ChartConfig> {
    return this._weatherStoreService.chartConfig$();
  }

  // pessimistic adding
  // 1. call API
  // 2. update UI state
  loadHourlyWeatherForecast(coordinates: Coordinates): void {
    this._weatherApiService.getHourlyForecast(coordinates)
      .subscribe(
        (hourForecastDetailsList: HourForecastDetails[]) => {
          this._weatherStoreService.setHoursForecast(hourForecastDetailsList);
          this._weatherStoreService.createChartConfigData();
        },
        (error) => console.log(error)
      );
  }

  // pessimistic adding
  // 1. call API
  // 2. update UI state
  addCityWeatherDetails(cityName: string): void {
    this._weatherApiService.getCurrentCityWeather(cityName)
      .subscribe(
        (cityWeatherDetails: CityWeatherDetails) =>
          this._weatherStoreService.addCityWeatherDetails(cityWeatherDetails),
        (error) => console.log(error)
      );
  }

  clearCitiesWeatherList(): void {
    this._weatherStoreService.setCitiesWeather([]);
  }


}
