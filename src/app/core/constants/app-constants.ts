export class AppConstants {
  /**
   * API key used for retrieving weather data
   */
  static WEATHER_API_KEY = '6c2e9c93946de4f788476173e7dc2a86';
  static WEATHER_BASE_API = 'https://api.openweathermap.org/data/2.5/';
  static WEATHER_ICON_API = ' http://openweathermap.org/img/wn/';

  /**
   * Date formats
   */
  static DISPLAY_DATE_FORMAT = 'dd MMM yyyy HH:mm';

  /**
   * Chart constants
   */
  static DEFAULT_CHART_COLOR = '#5bc0de';
}
