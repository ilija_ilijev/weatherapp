import {CityWeatherDetailsBuilder} from './builders/city-weather-details-builder';
import {Coordinates} from './coordinates';

export class CityWeatherDetails {
  private readonly _dt: number;
  private readonly _coordinates: Coordinates;
  private readonly _weather: string;
  private readonly _averageTemperature: number;
  private readonly _windSpeed: number;
  private readonly _name: string;
  private readonly _country: string;
  private readonly _icon: string;

  constructor(builder: CityWeatherDetailsBuilder) {
    this._dt = builder.dt;
    this._coordinates = builder.coordinates;
    this._weather = builder.weather;
    this._averageTemperature = builder.averageTemperature;
    this._windSpeed = builder.windSpeed;
    this._name = builder.name;
    this._country = builder.country;
    this._icon = builder.icon;
  }

  get icon(): string {
    return this._icon;
  }

  get coordinates(): Coordinates {
    return this._coordinates;
  }

  get dt(): number {
    return this._dt;
  }

  get weather(): string {
    return this._weather;
  }

  get averageTemperature(): number {
    return this._averageTemperature;
  }

  get windSpeed(): number {
    return this._windSpeed;
  }

  get name(): string {
    return this._name;
  }

  get country(): string {
    return this._country;
  }
}
