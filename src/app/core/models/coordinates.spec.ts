import {Coordinates} from './coordinates';

describe('Coordinates', () => {
  it('should create an instance', () => {
    expect(new Coordinates()).toBeTruthy();
  });

  it('should set properly the latitude property', () => {
    const coordinates = new Coordinates();
    coordinates.lat = 14;
    expect(coordinates.lat).toEqual(14);
  });

  it('should set properly the longitude property', () => {
    const coordinates = new Coordinates();
    coordinates.lon = 59;
    expect(coordinates.lon).toEqual(59);
  });
});
