import {ChartConfigBuilder} from './builders/chart-config-builder';

export class ChartConfig {
  private readonly _responsive: boolean;
  private readonly _labels: string[];
  private readonly _chartTitle: string;
  private readonly _fill: boolean;
  private readonly _chartData: any[];
  private readonly _showLegend: boolean;
  private readonly _chartType: 'line' | 'bar' | 'radar' | 'doughnut' | 'polarArea' | 'pie' | 'scatter';
  private readonly _lineRounded: boolean;
  private readonly _chartColor: string;


  constructor(builder: ChartConfigBuilder) {
    this._responsive = builder.responsive;
    this._labels = builder.labels;
    this._chartTitle = builder.chartTitle;
    this._fill = builder.fill;
    this._chartData = builder.chartData;
    this._showLegend = builder.showLegend;
    this._chartType = builder.chartType;
    this._lineRounded = builder.lineRounded;
    this._chartColor = builder.chartColor;
  }
  get responsive(): boolean {
    return this._responsive;
  }

  get labels(): string[] {
    return this._labels;
  }

  get chartTitle(): string {
    return this._chartTitle;
  }

  get fill(): boolean {
    return this._fill;
  }

  get chartData(): any[] {
    return this._chartData;
  }

  get showLegend(): boolean {
    return this._showLegend;
  }

  get chartType(): 'line' | 'bar' | 'radar' | 'doughnut' | 'polarArea' | 'pie' | 'scatter' {
    return this._chartType;
  }

  get lineRounded(): boolean {
    return this._lineRounded;
  }

  get chartColor(): string {
    return this._chartColor;
  }
}
