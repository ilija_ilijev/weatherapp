export class Coordinates {
  private _lat: number;
  private _lon: number;


  constructor() {
  }

  get lat(): number {
    return this._lat;
  }

  set lat(value: number) {
    this._lat = value;
  }

  get lon(): number {
    return this._lon;
  }

  set lon(value: number) {
    this._lon = value;
  }
}
