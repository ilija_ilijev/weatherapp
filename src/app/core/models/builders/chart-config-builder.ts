import {ChartConfig} from '../chart-config';

/**
 * Builder design pattern for ChartConfigBuilder class
 * For complex classes which have my properties this approach is the best
 */
export class ChartConfigBuilder {
  private _responsive: boolean;
  private _labels: string[];
  private _chartTitle: string;
  private _fill: boolean;
  private _chartData: any[];
  private _showLegend: boolean;
  private _chartType: 'line' | 'bar' | 'radar' | 'doughnut' | 'polarArea' | 'pie' | 'scatter';
  private _lineRounded: boolean;
  private _chartColor: string;

  get responsive(): boolean {
    return this._responsive;
  }

  get labels(): string[] {
    return this._labels;
  }

  get chartTitle(): string {
    return this._chartTitle;
  }

  get fill(): boolean {
    return this._fill;
  }

  get chartData(): any[] {
    return this._chartData;
  }

  get showLegend(): boolean {
    return this._showLegend;
  }

  get chartType(): 'line' | 'bar' | 'radar' | 'doughnut' | 'polarArea' | 'pie' | 'scatter' {
    return this._chartType;
  }

  get lineRounded(): boolean {
    return this._lineRounded;
  }

  get chartColor(): string {
    return this._chartColor;
  }

  setResponsive(responsive: boolean): ChartConfigBuilder {
    this._responsive = responsive;
    return this;
  }

  setLabels(labels: string[]): ChartConfigBuilder {
    this._labels = labels;
    return this;
  }

  setChartTitle(chartTitle: string): ChartConfigBuilder {
    this._chartTitle = chartTitle;
    return this;
  }

  setFill(fill: boolean): ChartConfigBuilder {
    this._fill = fill;
    return this;
  }

  setChartData(chartData: any[]): ChartConfigBuilder {
    this._chartData = chartData;
    return this;
  }

  setShowLegend(showLegend: boolean): ChartConfigBuilder {
    this._showLegend = showLegend;
    return this;
  }

  setChartType(chartType: 'line' | 'bar' | 'radar' | 'doughnut' | 'polarArea' | 'pie' | 'scatter'): ChartConfigBuilder {
    this._chartType = chartType;
    return this;
  }

  setLineRounded(lineRounded: boolean): ChartConfigBuilder {
    this._lineRounded = lineRounded;
    return this;
  }

  setChartColor(chartColor: string): ChartConfigBuilder {
    this._chartColor = chartColor;
    return this;
  }

  build(): ChartConfig {
    return new ChartConfig(this);
  }
}
