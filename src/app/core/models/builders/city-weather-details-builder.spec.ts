import {CityWeatherDetailsBuilder} from './city-weather-details-builder';
import {Coordinates} from '../coordinates';

describe('CityWeatherDetailsBuilder', () => {
  it('should create an instance', () => {
    expect(new CityWeatherDetailsBuilder()).toBeTruthy();
  });

  it('should create an instance with all properties defined', () => {
    const cityWeatherDetailsBuilder = new CityWeatherDetailsBuilder()
      .setName('Skopje')
      .setCoordinates(new Coordinates())
      .setWeather('Sunny')
      .setDt(121222)
      .setAverageTemperature(10)
      .setCountry('MK')
      .setWindSpeed(10);

    // Assertions
    expect(cityWeatherDetailsBuilder.name).toEqual('Skopje');
    expect(cityWeatherDetailsBuilder.coordinates).toEqual(new Coordinates());
    expect(cityWeatherDetailsBuilder.weather).toEqual('Sunny');
    expect(cityWeatherDetailsBuilder.dt).toEqual(121222);
    expect(cityWeatherDetailsBuilder.averageTemperature).toEqual(10);
    expect(cityWeatherDetailsBuilder.country).toEqual('MK');
    expect(cityWeatherDetailsBuilder.windSpeed).toEqual(10);
  });
});
