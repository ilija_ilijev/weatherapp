import {CityWeatherDetails} from '../city-weather-details';
import {Coordinates} from '../coordinates';

/**
 * Builder design pattern for CityWeatherDetails class
 * For complex classes which have my properties this approach is the best
 */
export class CityWeatherDetailsBuilder {
  private _dt: number;
  private _coordinates: Coordinates;
  private _weather: string;
  private _averageTemperature: number;
  private _windSpeed: number;
  private _name: string;
  private _country: string;
  private _icon: string;

  constructor() {
  }

  get icon(): string {
    return this._icon;
  }

  get coordinates(): Coordinates {
    return this._coordinates;
  }

  get dt(): number {
    return this._dt;
  }

  get weather(): string {
    return this._weather;
  }

  get averageTemperature(): number {
    return this._averageTemperature;
  }

  get windSpeed(): number {
    return this._windSpeed;
  }

  get name(): string {
    return this._name;
  }

  get country(): string {
    return this._country;
  }

  setDt(dateTimestamp: number): CityWeatherDetailsBuilder {
    this._dt = dateTimestamp;
    return this;
  }

  setCoordinates(coordinates: Coordinates): CityWeatherDetailsBuilder {
    this._coordinates = coordinates;
    return this;
  }

  setWeather(weather: string): CityWeatherDetailsBuilder {
    this._weather = weather;
    return this;
  }

  setAverageTemperature(averageTemperature: number): CityWeatherDetailsBuilder {
    this._averageTemperature = averageTemperature;
    return this;
  }

  setWindSpeed(windSpeed: number): CityWeatherDetailsBuilder {
    this._windSpeed = windSpeed;
    return this;
  }

  setName(name: string): CityWeatherDetailsBuilder {
    this._name = name;
    return this;
  }

  setCountry(country: string): CityWeatherDetailsBuilder {
    this._country = country;
    return this;
  }

  setIcon(icon: string): CityWeatherDetailsBuilder {
    this._icon = icon;
    return this;
  }

  build(): CityWeatherDetails {
    return new CityWeatherDetails(this);
  }
}
