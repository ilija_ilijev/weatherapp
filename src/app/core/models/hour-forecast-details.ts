export class HourForecastDetails {
  private _dt: number;
  private _temperature: number;

  constructor(dateTimestamp: number, temperature: number) {
    this._dt = dateTimestamp;
    this._temperature = temperature;
  }

  get dt(): number {
    return this._dt;
  }

  set dt(value: number) {
    this._dt = value;
  }

  get temperature(): number {
    return this._temperature;
  }

  set temperature(value: number) {
    this._temperature = value;
  }
}
