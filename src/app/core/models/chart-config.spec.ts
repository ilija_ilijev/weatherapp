import {ChartConfig} from './chart-config';
import {ChartConfigBuilder} from './builders/chart-config-builder';

describe('ChartConfig', () => {
  it('should create an instance', () => {
    expect(new ChartConfigBuilder().build()).toBeTruthy();
  });
  it('should create complex instance', () => {
    const chartConfig = new ChartConfigBuilder()
      .setShowLegend(true)
      .setResponsive(true)
      .setLineRounded(true)
      .setFill(true)
      .setChartTitle('ChartTitle')
      .setChartType('line')
      .setChartColor('red')
      .build() as ChartConfig;

    expect(chartConfig.showLegend).toEqual(true);
    expect(chartConfig.responsive).toEqual(true);
    expect(chartConfig.lineRounded).toEqual(true);
    expect(chartConfig.fill).toEqual(true);
    expect(chartConfig.chartType).toEqual('line');
    expect(chartConfig.chartColor).toEqual('red');
    expect(chartConfig.chartTitle).toEqual('ChartTitle');
  });
});
