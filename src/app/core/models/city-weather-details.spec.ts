import {CityWeatherDetails} from './city-weather-details';
import {CityWeatherDetailsBuilder} from './builders/city-weather-details-builder';

describe('CityWeatherDetails', () => {
  it('should create an instance', () => {
    expect(new CityWeatherDetails(new CityWeatherDetailsBuilder())).toBeTruthy();
  });

  it('should create an object with only name set', () => {
    const cityWeatherDetails = new CityWeatherDetails(new CityWeatherDetailsBuilder().setName('Skopje'));

    // Assertions
    expect(cityWeatherDetails.name).toEqual('Skopje');
    expect(cityWeatherDetails.coordinates).toBeUndefined();
    expect(cityWeatherDetails.windSpeed).toBeUndefined();
    expect(cityWeatherDetails.weather).toBeUndefined();
    expect(cityWeatherDetails.averageTemperature).toBeUndefined();
    expect(cityWeatherDetails.country).toBeUndefined();
    expect(cityWeatherDetails.dt).toBeUndefined();
  });
});
