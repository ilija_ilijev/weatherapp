/**
 * Available options for units are:
 *  * For temperature in Kelvin: 'standard'
 *  * For temperature in Fahrenheit: 'imperial'
 *  * For temperature in Celsius: 'metric'
 */
export enum UnitsEnum {
  STANDARD = 'standard',
  METRIC = 'metric',
  IMPERIAL = 'imperial'
}
