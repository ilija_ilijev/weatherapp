import {TestBed} from '@angular/core/testing';

import {WeatherApiService} from './weather-api.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Coordinates} from '../models/coordinates';

describe('WeatherApiService', () => {
  // We declare the variables that we'll use for the Test Controller and for our Service
  let httpTestingController: HttpTestingController;
  let service: WeatherApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: []
    });
    service = TestBed.inject(WeatherApiService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve Hourly forecast data', () => {

    const mockHistoryWeatherDetailsData = {
      lat: 42,
      lon: 21.43,
      timezone: 'Europe/Skopje',
      timezone_offset: 3600,
      hourly: [
        {
          dt: 1607281200,
          temp: 9.54,
          feels_like: 7.21,
          pressure: 1014,
          humidity: 81,
          dew_point: 6.45,
          uvi: 0,
          clouds: 75,
          visibility: 10000,
          wind_speed: 2.15,
          wind_deg: 109,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607284800,
          temp: 9.34,
          feels_like: 6.88,
          pressure: 1014,
          humidity: 84,
          dew_point: 6.78,
          uvi: 0,
          clouds: 66,
          visibility: 10000,
          wind_speed: 2.44,
          wind_deg: 103,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607288400,
          temp: 9.09,
          feels_like: 6.99,
          pressure: 1013,
          humidity: 85,
          dew_point: 6.71,
          uvi: 0,
          clouds: 62,
          visibility: 10000,
          wind_speed: 1.91,
          wind_deg: 119,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04n'
            }
          ],
          pop: 0.01
        },
        {
          dt: 1607292000,
          temp: 9,
          feels_like: 7,
          pressure: 1012,
          humidity: 86,
          dew_point: 6.79,
          uvi: 0,
          clouds: 67,
          visibility: 10000,
          wind_speed: 1.79,
          wind_deg: 109,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607295600,
          temp: 8.92,
          feels_like: 6.5,
          pressure: 1011,
          humidity: 86,
          dew_point: 6.71,
          uvi: 0,
          clouds: 70,
          visibility: 10000,
          wind_speed: 2.36,
          wind_deg: 101,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607299200,
          temp: 8.85,
          feels_like: 6.15,
          pressure: 1010,
          humidity: 86,
          dew_point: 6.75,
          uvi: 0,
          clouds: 72,
          visibility: 10000,
          wind_speed: 2.75,
          wind_deg: 99,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607302800,
          temp: 8.33,
          feels_like: 5.79,
          pressure: 1010,
          humidity: 88,
          dew_point: 6.49,
          uvi: 0,
          clouds: 88,
          visibility: 10000,
          wind_speed: 2.46,
          wind_deg: 103,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0.09
        },
        {
          dt: 1607306400,
          temp: 8.29,
          feels_like: 5.02,
          pressure: 1010,
          humidity: 87,
          dew_point: 6.36,
          uvi: 0,
          clouds: 94,
          visibility: 10000,
          wind_speed: 3.44,
          wind_deg: 100,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0.34
        },
        {
          dt: 1607310000,
          temp: 8.94,
          feels_like: 5.16,
          pressure: 1009,
          humidity: 84,
          dew_point: 6.44,
          uvi: 0,
          clouds: 96,
          visibility: 10000,
          wind_speed: 4.21,
          wind_deg: 101,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0.35
        },
        {
          dt: 1607313600,
          temp: 8.85,
          feels_like: 4.82,
          pressure: 1009,
          humidity: 84,
          dew_point: 6.31,
          uvi: 0,
          clouds: 97,
          visibility: 10000,
          wind_speed: 4.53,
          wind_deg: 98,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10n'
            }
          ],
          pop: 0.43,
          rain: {
            '1h': 0.11
          }
        },
        {
          dt: 1607317200,
          temp: 8.98,
          feels_like: 5.16,
          pressure: 1009,
          humidity: 83,
          dew_point: 6.29,
          uvi: 0,
          clouds: 98,
          visibility: 10000,
          wind_speed: 4.22,
          wind_deg: 97,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10n'
            }
          ],
          pop: 0.47,
          rain: {
            '1h': 0.18
          }
        },
        {
          dt: 1607320800,
          temp: 8.76,
          feels_like: 5.14,
          pressure: 1009,
          humidity: 85,
          dew_point: 6.41,
          uvi: 0,
          clouds: 98,
          visibility: 10000,
          wind_speed: 3.98,
          wind_deg: 91,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10d'
            }
          ],
          pop: 0.58,
          rain: {
            '1h': 0.17
          }
        },
        {
          dt: 1607324400,
          temp: 8.79,
          feels_like: 5.52,
          pressure: 1009,
          humidity: 86,
          dew_point: 6.67,
          uvi: 0.02,
          clouds: 100,
          visibility: 10000,
          wind_speed: 3.54,
          wind_deg: 85,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10d'
            }
          ],
          pop: 0.73,
          rain: {
            '1h': 0.24
          }
        },
        {
          dt: 1607328000,
          temp: 8.62,
          feels_like: 5.93,
          pressure: 1009,
          humidity: 88,
          dew_point: 6.86,
          uvi: 0.06,
          clouds: 100,
          visibility: 6154,
          wind_speed: 2.76,
          wind_deg: 71,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10d'
            }
          ],
          pop: 0.93,
          rain: {
            '1h': 0.99
          }
        },
        {
          dt: 1607331600,
          temp: 8.75,
          feels_like: 6.5,
          pressure: 1009,
          humidity: 88,
          dew_point: 6.9,
          uvi: 0.12,
          clouds: 100,
          visibility: 9374,
          wind_speed: 2.17,
          wind_deg: 63,
          weather: [
            {
              id: 501,
              main: 'Rain',
              description: 'moderate rain',
              icon: '10d'
            }
          ],
          pop: 1,
          rain: {
            '1h': 1.04
          }
        },
        {
          dt: 1607335200,
          temp: 8.52,
          feels_like: 6.16,
          pressure: 1009,
          humidity: 90,
          dew_point: 7.13,
          uvi: 0.2,
          clouds: 100,
          visibility: 3111,
          wind_speed: 2.36,
          wind_deg: 86,
          weather: [
            {
              id: 501,
              main: 'Rain',
              description: 'moderate rain',
              icon: '10d'
            }
          ],
          pop: 1,
          rain: {
            '1h': 3.87
          }
        },
        {
          dt: 1607338800,
          temp: 8.72,
          feels_like: 6.8,
          pressure: 1009,
          humidity: 90,
          dew_point: 7.19,
          uvi: 0.2,
          clouds: 100,
          visibility: 2970,
          wind_speed: 1.8,
          wind_deg: 86,
          weather: [
            {
              id: 501,
              main: 'Rain',
              description: 'moderate rain',
              icon: '10d'
            }
          ],
          pop: 1,
          rain: {
            '1h': 3.4
          }
        },
        {
          dt: 1607342400,
          temp: 8.75,
          feels_like: 6.93,
          pressure: 1009,
          humidity: 89,
          dew_point: 7.16,
          uvi: 0.16,
          clouds: 100,
          visibility: 10000,
          wind_speed: 1.61,
          wind_deg: 43,
          weather: [
            {
              id: 501,
              main: 'Rain',
              description: 'moderate rain',
              icon: '10d'
            }
          ],
          pop: 1,
          rain: {
            '1h': 1.14
          }
        },
        {
          dt: 1607346000,
          temp: 8.75,
          feels_like: 7.33,
          pressure: 1009,
          humidity: 89,
          dew_point: 7.05,
          uvi: 0.12,
          clouds: 100,
          visibility: 10000,
          wind_speed: 1.05,
          wind_deg: 49,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10d'
            }
          ],
          pop: 0.83,
          rain: {
            '1h': 0.2
          }
        },
        {
          dt: 1607349600,
          temp: 8.65,
          feels_like: 7.36,
          pressure: 1009,
          humidity: 89,
          dew_point: 6.98,
          uvi: 0.04,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.82,
          wind_deg: 50,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04d'
            }
          ],
          pop: 0.83
        },
        {
          dt: 1607353200,
          temp: 8.41,
          feels_like: 7.07,
          pressure: 1010,
          humidity: 89,
          dew_point: 6.86,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.82,
          wind_deg: 62,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10d'
            }
          ],
          pop: 0.88,
          rain: {
            '1h': 0.15
          }
        },
        {
          dt: 1607356800,
          temp: 8.28,
          feels_like: 7.23,
          pressure: 1010,
          humidity: 90,
          dew_point: 6.81,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.42,
          wind_deg: 108,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10n'
            }
          ],
          pop: 0.88,
          rain: {
            '1h': 0.19
          }
        },
        {
          dt: 1607360400,
          temp: 8.28,
          feels_like: 7.09,
          pressure: 1010,
          humidity: 90,
          dew_point: 6.84,
          uvi: 0,
          clouds: 100,
          visibility: 7593,
          wind_speed: 0.62,
          wind_deg: 291,
          weather: [
            {
              id: 501,
              main: 'Rain',
              description: 'moderate rain',
              icon: '10n'
            }
          ],
          pop: 1,
          rain: {
            '1h': 1.12
          }
        },
        {
          dt: 1607364000,
          temp: 8.34,
          feels_like: 7.13,
          pressure: 1011,
          humidity: 90,
          dew_point: 6.86,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.67,
          wind_deg: 279,
          weather: [
            {
              id: 501,
              main: 'Rain',
              description: 'moderate rain',
              icon: '10n'
            }
          ],
          pop: 1,
          rain: {
            '1h': 1.54
          }
        },
        {
          dt: 1607367600,
          temp: 8.4,
          feels_like: 7.46,
          pressure: 1012,
          humidity: 89,
          dew_point: 6.78,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.25,
          wind_deg: 77,
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10n'
            }
          ],
          pop: 0.68,
          rain: {
            '1h': 0.62
          }
        },
        {
          dt: 1607371200,
          temp: 8.19,
          feels_like: 6.97,
          pressure: 1012,
          humidity: 89,
          dew_point: 6.57,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.58,
          wind_deg: 30,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0.62
        },
        {
          dt: 1607374800,
          temp: 7.97,
          feels_like: 6.54,
          pressure: 1013,
          humidity: 89,
          dew_point: 6.31,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.82,
          wind_deg: 103,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0.62
        },
        {
          dt: 1607378400,
          temp: 7.62,
          feels_like: 6.01,
          pressure: 1013,
          humidity: 89,
          dew_point: 6.04,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.97,
          wind_deg: 116,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0.62
        },
        {
          dt: 1607382000,
          temp: 7.59,
          feels_like: 6.23,
          pressure: 1013,
          humidity: 88,
          dew_point: 5.89,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.55,
          wind_deg: 99,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0.62
        },
        {
          dt: 1607385600,
          temp: 7.38,
          feels_like: 6.31,
          pressure: 1013,
          humidity: 87,
          dew_point: 5.49,
          uvi: 0,
          clouds: 100,
          visibility: 10000,
          wind_speed: 0.03,
          wind_deg: 149,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0.58
        },
        {
          dt: 1607389200,
          temp: 7.12,
          feels_like: 5.7,
          pressure: 1014,
          humidity: 86,
          dew_point: 4.95,
          uvi: 0,
          clouds: 73,
          visibility: 10000,
          wind_speed: 0.41,
          wind_deg: 291,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607392800,
          temp: 6.15,
          feels_like: 4.05,
          pressure: 1014,
          humidity: 83,
          dew_point: 3.65,
          uvi: 0,
          clouds: 46,
          visibility: 10000,
          wind_speed: 0.98,
          wind_deg: 287,
          weather: [
            {
              id: 802,
              main: 'Clouds',
              description: 'scattered clouds',
              icon: '03n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607396400,
          temp: 5.78,
          feels_like: 3.24,
          pressure: 1014,
          humidity: 80,
          dew_point: 2.64,
          uvi: 0,
          clouds: 34,
          visibility: 10000,
          wind_speed: 1.39,
          wind_deg: 281,
          weather: [
            {
              id: 802,
              main: 'Clouds',
              description: 'scattered clouds',
              icon: '03n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607400000,
          temp: 5.35,
          feels_like: 2.61,
          pressure: 1014,
          humidity: 76,
          dew_point: 1.51,
          uvi: 0,
          clouds: 32,
          visibility: 10000,
          wind_speed: 1.4,
          wind_deg: 282,
          weather: [
            {
              id: 802,
              main: 'Clouds',
              description: 'scattered clouds',
              icon: '03n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607403600,
          temp: 5.02,
          feels_like: 2.22,
          pressure: 1015,
          humidity: 73,
          dew_point: 0.77,
          uvi: 0,
          clouds: 29,
          visibility: 10000,
          wind_speed: 1.29,
          wind_deg: 286,
          weather: [
            {
              id: 802,
              main: 'Clouds',
              description: 'scattered clouds',
              icon: '03n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607407200,
          temp: 4.76,
          feels_like: 1.99,
          pressure: 1015,
          humidity: 72,
          dew_point: 0.28,
          uvi: 0,
          clouds: 25,
          visibility: 10000,
          wind_speed: 1.15,
          wind_deg: 288,
          weather: [
            {
              id: 802,
              main: 'Clouds',
              description: 'scattered clouds',
              icon: '03d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607410800,
          temp: 6.08,
          feels_like: 3.48,
          pressure: 1015,
          humidity: 68,
          dew_point: 0.63,
          uvi: 0.21,
          clouds: 0,
          visibility: 10000,
          wind_speed: 1.01,
          wind_deg: 286,
          weather: [
            {
              id: 800,
              main: 'Clear',
              description: 'clear sky',
              icon: '01d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607414400,
          temp: 7.74,
          feels_like: 5.57,
          pressure: 1015,
          humidity: 60,
          dew_point: 0.47,
          uvi: 0.62,
          clouds: 0,
          visibility: 10000,
          wind_speed: 0.36,
          wind_deg: 327,
          weather: [
            {
              id: 800,
              main: 'Clear',
              description: 'clear sky',
              icon: '01d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607418000,
          temp: 8.97,
          feels_like: 6.79,
          pressure: 1014,
          humidity: 57,
          dew_point: 1.13,
          uvi: 1.14,
          clouds: 1,
          visibility: 10000,
          wind_speed: 0.47,
          wind_deg: 50,
          weather: [
            {
              id: 800,
              main: 'Clear',
              description: 'clear sky',
              icon: '01d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607421600,
          temp: 9.79,
          feels_like: 7.05,
          pressure: 1014,
          humidity: 58,
          dew_point: 2.14,
          uvi: 1.53,
          clouds: 1,
          visibility: 10000,
          wind_speed: 1.5,
          wind_deg: 88,
          weather: [
            {
              id: 800,
              main: 'Clear',
              description: 'clear sky',
              icon: '01d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607425200,
          temp: 10.53,
          feels_like: 7.96,
          pressure: 1013,
          humidity: 59,
          dew_point: 3.01,
          uvi: 1.57,
          clouds: 1,
          visibility: 10000,
          wind_speed: 1.49,
          wind_deg: 95,
          weather: [
            {
              id: 800,
              main: 'Clear',
              description: 'clear sky',
              icon: '01d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607428800,
          temp: 11.08,
          feels_like: 8.57,
          pressure: 1013,
          humidity: 58,
          dew_point: 3.35,
          uvi: 1.25,
          clouds: 0,
          visibility: 10000,
          wind_speed: 1.47,
          wind_deg: 85,
          weather: [
            {
              id: 800,
              main: 'Clear',
              description: 'clear sky',
              icon: '01d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607432400,
          temp: 11.31,
          feels_like: 8.7,
          pressure: 1013,
          humidity: 58,
          dew_point: 3.55,
          uvi: 0.69,
          clouds: 24,
          visibility: 10000,
          wind_speed: 1.67,
          wind_deg: 87,
          weather: [
            {
              id: 801,
              main: 'Clouds',
              description: 'few clouds',
              icon: '02d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607436000,
          temp: 10.83,
          feels_like: 8.6,
          pressure: 1013,
          humidity: 63,
          dew_point: 4.08,
          uvi: 0.25,
          clouds: 62,
          visibility: 10000,
          wind_speed: 1.32,
          wind_deg: 89,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607439600,
          temp: 8.93,
          feels_like: 6.53,
          pressure: 1013,
          humidity: 67,
          dew_point: 3.31,
          uvi: 0,
          clouds: 75,
          visibility: 10000,
          wind_speed: 1.32,
          wind_deg: 88,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04d'
            }
          ],
          pop: 0
        },
        {
          dt: 1607443200,
          temp: 8.12,
          feels_like: 5.94,
          pressure: 1014,
          humidity: 70,
          dew_point: 3.03,
          uvi: 0,
          clouds: 81,
          visibility: 10000,
          wind_speed: 0.97,
          wind_deg: 96,
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607446800,
          temp: 7.65,
          feels_like: 5.46,
          pressure: 1014,
          humidity: 71,
          dew_point: 2.87,
          uvi: 0,
          clouds: 85,
          visibility: 10000,
          wind_speed: 0.91,
          wind_deg: 74,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0
        },
        {
          dt: 1607450400,
          temp: 7.2,
          feels_like: 5.27,
          pressure: 1014,
          humidity: 73,
          dew_point: 2.77,
          uvi: 0,
          clouds: 87,
          visibility: 10000,
          wind_speed: 0.54,
          wind_deg: 34,
          weather: [
            {
              id: 804,
              main: 'Clouds',
              description: 'overcast clouds',
              icon: '04n'
            }
          ],
          pop: 0
        }
      ]
    };

    const coordinates = new Coordinates();
    coordinates.lat = 42;
    coordinates.lon = 21.43;
    service.getHourlyForecast(coordinates)
      .subscribe(hourForecastDetails => {
        expect(hourForecastDetails[0].dt).toEqual(1607281200);
        expect(hourForecastDetails[0].temperature).toEqual(9.54);
        expect(hourForecastDetails[1].dt).toEqual(1607284800);
        expect(hourForecastDetails[1].temperature).toEqual(9.34);
        expect(hourForecastDetails[2].dt).toEqual(1607288400);
        expect(hourForecastDetails[2].temperature).toEqual(9.09);
      });

    const req = httpTestingController.expectOne(
      'https://api.openweathermap.org/data/2.5/onecall?lat=42&lon=21.43&units=metric&exclude=minutely,daily,current'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockHistoryWeatherDetailsData);
  });


  it('should retrieve current weather details data', () => {

    const mockCurrentWeather: any = {
      coord: {
        lon: 21.43,
        lat: 42
      },
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04n'
        }
      ],
      base: 'stations',
      main: {
        temp: 2,
        feels_like: -0.54,
        temp_min: 2,
        temp_max: 4.7,
        pressure: 1018,
        humidity: 93
      },
      visibility: 10000,
      wind: {
        speed: 1,
        deg: 270
      },
      clouds: {
        all: 75
      },
      dt: 1607222517,
      sys: {
        type: 1,
        id: 7023,
        country: 'MK',
        sunrise: 1607233691,
        sunset: 1607266982
      },
      timezone: 3600,
      id: 785842,
      name: 'Skopje',
      cod: 200
    };

    service.getCurrentCityWeather('Skopje')
      .subscribe(cityWeatherDetails => {
        expect(cityWeatherDetails.country).toEqual('MK');
        expect(cityWeatherDetails.name).toEqual('Skopje');
        expect(cityWeatherDetails.weather).toEqual('Clouds');
        expect(cityWeatherDetails.windSpeed).toEqual(1);
        expect(cityWeatherDetails.dt).toEqual(1607222517);
      });

    const req = httpTestingController.expectOne(
      'https://api.openweathermap.org/data/2.5/weather?q=Skopje&units=metric'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockCurrentWeather);
  });
});
