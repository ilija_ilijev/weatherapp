import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CityWeatherDetails} from '../models/city-weather-details';
import {CityWeatherDetailsBuilder} from '../models/builders/city-weather-details-builder';
import {HourForecastDetails} from '../models/hour-forecast-details';
import {Coordinates} from '../models/coordinates';
import {AppConstants} from '../constants/app-constants';
import {UnitsEnum} from '../models/units.enum';
import {map} from 'rxjs/operators';

/**
 * API service which consumes the data from the defined REST API.
 * It is part of Core layer and its task is just to retrieve details from the API,
 * no other logic (we can cache some responses if we want too)
 */
@Injectable({
  providedIn: 'root'
})
export class WeatherApiService {
  constructor(private httpClient: HttpClient) {
  }

  getCurrentCityWeather(cityName: string): Observable<CityWeatherDetails> {
    return this.httpClient.get(AppConstants.WEATHER_BASE_API + 'weather', {
      params: {
        q: cityName,
        units: UnitsEnum.METRIC
      }
    }).pipe(map((response: any) => {
      return new CityWeatherDetailsBuilder()
        .setDt(response?.dt)
        .setCoordinates(response?.coord)
        .setAverageTemperature((response?.main?.temp_min + response?.main?.temp_min) / 2)
        .setName(response?.name)
        .setWeather(response?.weather[0]?.main)
        .setIcon(AppConstants.WEATHER_ICON_API + response?.weather[0]?.icon + '.png')
        .setWindSpeed(response?.wind?.speed)
        .setCountry(response?.sys?.country)
        .build();
    }));
  }

  getHourlyForecast(coordinates: Coordinates): Observable<HourForecastDetails[]> {
    return this.httpClient.get(AppConstants.WEATHER_BASE_API + 'onecall', {
      params: {
        lat: coordinates?.lat?.toString(),
        lon: coordinates?.lon?.toString(),
        units: UnitsEnum.METRIC,
        exclude: 'minutely,daily,current'
      }
    }).pipe(map((response: any) => {
      return response?.hourly?.slice(0, 12).map(item => {
        return new HourForecastDetails(item?.dt, item?.temp);
      });
    }));

  }
}
