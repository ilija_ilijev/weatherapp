import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {ApiKeyInterceptorProvider} from './interceptors/api-key.interceptor';
import {SharedModule} from '../shared/shared.module';

export function throwIfAlreadyLoaded(parentModule: any, moduleName: string): void {
  if (parentModule) {
    throw new Error(`${moduleName} has already been loaded. Import Core modules in AppModule only`);
  }
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [
    ApiKeyInterceptorProvider
  ]
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule
  ) {
    /**
     * Check for double importing of the module - CoreModules should be imported only once, in root module(AppModule)
     */
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
