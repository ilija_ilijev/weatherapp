import {async, TestBed} from '@angular/core/testing';

import {WeatherStateService} from './weather-state.service';
import {DatePipe} from '@angular/common';
import {CityWeatherDetails} from '../models/city-weather-details';
import {HourForecastDetails} from '../models/hour-forecast-details';
import {ChartConfig} from '../models/chart-config';
import {AppConstants} from '../constants/app-constants';
import {ChartConfigBuilder} from '../models/builders/chart-config-builder';

describe('WeatherStoreService', () => {
  let service: WeatherStateService;
  let datePipe: DatePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatePipe]
    });
    service = TestBed.inject(WeatherStateService);
    datePipe = TestBed.inject(DatePipe);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should has default cities weather data set', (done) => {

    service.citiesWeather$().subscribe((state) => {
      expect(state).toEqual([]);
      done();
    });
  });

  it('should has default chart config data set', (done) => {
    service.chartConfig$().subscribe((state) => {
      expect(state).toEqual(null);
      done();
    });
  });

  it('should has default hourly forecast data  set', (done) => {
    service.hoursForecast$().subscribe((state) => {
      expect(state).toEqual([]);
      done();
    });
  });

  it('should add city weather details', async(() => {
    const cityWeatherDetails: CityWeatherDetails = {
      name: 'Skopje',
      country: 'MK',
      coordinates: {
        lat: 42,
        lon: 21.43
      },
      weather: 'Clear',
      windSpeed: 10,
      averageTemperature: 10,
      dt: 1121212
    } as CityWeatherDetails;

    service.addCityWeatherDetails(cityWeatherDetails);
    service.citiesWeather$().subscribe(cityWeatherDetailsData => {
      expect(cityWeatherDetailsData).toEqual([cityWeatherDetails]);
    });
  }));

  it('should set city weather details', async(() => {
    const cityWeatherDetails: CityWeatherDetails[] = [{
      name: 'Skopje',
      country: 'MK',
      coordinates: {
        lat: 42,
        lon: 21.43
      },
      weather: 'Clear',
      windSpeed: 10,
      averageTemperature: 10,
      dt: 1121212
    }] as CityWeatherDetails[];

    service.setCitiesWeather(cityWeatherDetails);
    service.citiesWeather$().subscribe(cityWeatherDetailsData => {
      expect(cityWeatherDetailsData).toEqual(cityWeatherDetails);
    });
  }));

  it('should set hour forecast details', async(() => {
    const hourForecastDetails: HourForecastDetails[] = [{
      dt: 1121212,
      temperature: 7
    }, {
      dt: 1121315,
      temperature: 10
    }
    ] as HourForecastDetails[];

    service.setHoursForecast(hourForecastDetails);
    service.hoursForecast$().subscribe(hourForecastDetailsData => {
      expect(hourForecastDetailsData).toEqual(hourForecastDetails);
    });
  }));

  it('should create charts config data', async(() => {
    const hourForecastDetails: HourForecastDetails[] = [{
      dt: 1607383680,
      temperature: 7
    }, {
      dt: 1607382000,
      temperature: 10
    }
    ] as HourForecastDetails[];

    const chartConfigData: ChartConfig = new ChartConfigBuilder()
      .setChartColor(AppConstants.DEFAULT_CHART_COLOR)
      .setChartData([7, 10])
      .setLabels([
        datePipe.transform(1607383680 * 1000, AppConstants.DISPLAY_DATE_FORMAT),
        datePipe.transform(1607382000 * 1000, AppConstants.DISPLAY_DATE_FORMAT)
      ])
      .setChartType('line')
      .setChartTitle('Temperature in Celsius degrees')
      .setFill(false)
      .setLineRounded(false)
      .setResponsive(true)
      .setShowLegend(true)
      .build();

    service.setHoursForecast(hourForecastDetails);
    service.createChartConfigData();
    service.chartConfig$().subscribe(chartConfig => {
      expect(chartConfig).toEqual(chartConfigData);
    });
  }));
});
