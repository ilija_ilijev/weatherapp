import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {CityWeatherDetails} from '../models/city-weather-details';
import {HourForecastDetails} from '../models/hour-forecast-details';
import {ChartConfig} from '../models/chart-config';
import {DatePipe} from '@angular/common';
import {AppConstants} from '../constants/app-constants';
import {ChartConfigBuilder} from '../models/builders/chart-config-builder';
import {DomSanitizer} from '@angular/platform-browser';

/**
 * Store for saving weathers state - it is part of Core layer and it is implemented with BehaviourSubjects
 *
 * Another approach for store is Redux design pattern,
 * but because the applications is too small it is better to go with BehaviourSubjects way
 *
 * Because the application follows Separation of Concerns (SoC) principle
 * it is very easy to replace this store with Redux (NgRx implementation),
 * that change will not make any implications on Presentation layer.
 */
@Injectable({
  providedIn: 'root'
})
export class WeatherStateService {

  private _citiesWeather$ = new BehaviorSubject<CityWeatherDetails[]>([]);
  private _hoursForecast$ = new BehaviorSubject<HourForecastDetails[]>([]);
  private _chartConfig$ = new BehaviorSubject<ChartConfig>(null);

  constructor(private _datePipe: DatePipe,
              private _sanitizer: DomSanitizer) {
  }

  citiesWeather$(): Observable<CityWeatherDetails[]> {
    return this._citiesWeather$.asObservable();
  }

  hoursForecast$(): Observable<HourForecastDetails[]> {
    return this._hoursForecast$.asObservable();
  }

  chartConfig$(): Observable<ChartConfig> {
    return this._chartConfig$.asObservable();
  }

  setCitiesWeather(citiesWeather: CityWeatherDetails[]): void {
    this._citiesWeather$.next(citiesWeather);
  }

  addCityWeatherDetails(cityWeatherDetails: CityWeatherDetails): void {
    const currentValue = this._citiesWeather$.getValue();
    this._citiesWeather$.next([...currentValue, cityWeatherDetails]);
  }

  setHoursForecast(hoursForecast: HourForecastDetails[]): void {
    this._hoursForecast$.next(hoursForecast);
  }

  createChartConfigData(): void {
    const hourForecastData: HourForecastDetails[] = this._hoursForecast$.getValue();
    const chartLabels: string[] = [];
    const chartData: any[] = [];
    hourForecastData.forEach((forecast => {
      chartLabels.push(this._datePipe.transform(forecast.dt * 1000, AppConstants.DISPLAY_DATE_FORMAT));
      chartData.push(forecast.temperature);
    }));

    this._chartConfig$.next(new ChartConfigBuilder()
      .setChartColor(AppConstants.DEFAULT_CHART_COLOR)
      .setChartData(chartData)
      .setLabels(chartLabels)
      .setChartType('line')
      .setChartTitle('Temperature in Celsius degrees')
      .setFill(false)
      .setLineRounded(false)
      .setResponsive(true)
      .setShowLegend(true)
      .build());


  }
}
