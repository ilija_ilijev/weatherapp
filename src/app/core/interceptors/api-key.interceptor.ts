import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AppConstants} from '../constants/app-constants';

@Injectable()
export class ApiKeyInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let newRequest: HttpRequest<any> = request.clone();
    /**
     *  Attaching WEATHER_API_KEY on every request to 'openweathermap.org'
     */
    if (request.url.startsWith(AppConstants.WEATHER_BASE_API)) {
      newRequest = newRequest.clone({
        params: request.params.append('appid', AppConstants.WEATHER_API_KEY)
      });
    }

    return next.handle(newRequest);
  }
}
export const ApiKeyInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: ApiKeyInterceptor,
  multi: true
};
