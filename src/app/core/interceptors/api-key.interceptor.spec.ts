import {TestBed} from '@angular/core/testing';

import {ApiKeyInterceptor} from './api-key.interceptor';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {WeatherApiService} from '../api-services/weather-api.service';

describe('ApiKeyInterceptor', () => {
  let service: WeatherApiService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        WeatherApiService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ApiKeyInterceptor,
          multi: true
        },
        ApiKeyInterceptor
      ]
    });
    service = TestBed.inject(WeatherApiService);
    httpTestingController = TestBed.inject(HttpTestingController);

  });

  it('should be created', () => {
    const interceptor: ApiKeyInterceptor = TestBed.inject(ApiKeyInterceptor);
    expect(interceptor).toBeTruthy();
  });

  it('should have appid in query param', () => {
    service.getCurrentCityWeather('Skopje').subscribe(response => {
      expect(response).toBeTruthy();
    });
    const req = httpTestingController.expectOne(
      'https://api.openweathermap.org/data/2.5/weather?q=Skopje&units=metric&appid=6c2e9c93946de4f788476173e7dc2a86'
    );
    expect(req.request.params.has('appid')).toEqual(true);
  });
});
