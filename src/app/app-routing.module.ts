import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';


const appRoutes: Routes = [
  {
    path: 'weather',
    loadChildren: () => import('./features/weather/weather.module').then(m => m.WeatherModule),
  },
  {
    path: '**',
    redirectTo: 'weather'
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
